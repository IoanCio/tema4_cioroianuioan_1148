import React,{Component} from 'react'
import OrganizationStore from'./stores/OrganizationStore'
import OrganizationForm from './OrganizationForm'
import Organization from './Organization'
import OrganizationDetails from './OrganizationDetails'

class OrganizationList extends Component{
    constructor(){
        super()
        this.state = {
            organizations: [],
            detailsFor : -1,
			selected : null
        }
        this.store = new OrganizationStore()
        this.emitter = this.store.emitter
        this.add = (organization) => {
            this.store.addOne(organization)
        }
        this.delete = (id) => {
            this.store.deleteOne(id)
        }
        this.save = (id, organization) => {
            this.store.saveOne(id, organization)
        }
        this.showDetails = (id) => {
			this.store.getOne(id)
			this.store.emitter.addListener('GET_ONE_SUCCESS', () => {
				this.setState({
					selected : this.store.selected,
					detailsFor : id
				})
			})
		}
		this.hideDetails = () => {
			this.setState({
				detailsFor : -1
			})
		}
    }
    componentDidMount(){
        this.store.getAll()
        this.store.emitter.addListener('GET_ORGANIZATIONS_SUCCESS', () => {
            this.setState({
                organizations : this.store.content
            })
        })
    }
    render(){
        if (this.state.detailsFor === -1){
            return (
                <div className="title3">
                    {
                        this.state.organizations.map((e, i) => <Organization key={i} item={e} onSave={this.save} onDelete={this.delete} onSelect={this.showDetails}/>)
                    }
                    <OrganizationForm onAdd={this.add} />
                </div>
            )
        }
        else{
            return <OrganizationDetails item={this.state.selected} onCancel={this.hideDetails}/>
        }

          
    }
}

export default OrganizationList