import React, { Component } from 'react';
import FounderStore from './stores/FounderStore'
import Founder from './Founder'
import FounderAddForm from './FounderAddForm'

class OrganizationDetails extends Component {
	constructor(props){
		super(props)
		this.state = {
			founders : []
		}
		this.store = new FounderStore()
		this.emitter = this.store.emitter
		this.add = (founder) => {
			this.store.addOne(this.props.item.id, founder)
		}
		this.delete = (founderId) => {
			this.store.deleteOne(this.props.item.id, founderId)
		}
		this.save = (founderId, founder) => {
			this.store.saveOne(this.props.item.id, founderId, founder)
		}
	}
	componentDidMount(){
		this.store.getAll(this.props.item.id)
		this.store.emitter.addListener('GET_ALL_SUCCESS', () => {
			this.setState({
				founders : this.store.content
			})
		})
	}
  render() {
    return (
      <div>
        <h3>Founders for {this.props.item.name}</h3>
        <div>
        	{
        		this.state.founders.map((e, i) => <Founder item={e} key={i} onDelete={this.delete} onSave={this.save} />)
        	}
        </div>
        <div>
        	<FounderAddForm onAdd={this.add} />
        </div>
        <form>
        	<input type="button" value="back" onClick={() => this.props.onCancel()} />
        </form>
      </div>
    )
  }
}

export default OrganizationDetails
