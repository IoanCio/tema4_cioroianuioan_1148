import React, {Component} from 'react'

class OrganizationForm extends Component{
    constructor(props){
        super(props)
        this.state = {
            name : '',
            category : '',
            location : ''
        }
        this.handleChange = (evt) => {
            this.setState({
                [evt.target.name] : evt.target.value
            })
        }
        this.add = () => {
            this.props.onAdd({
                name : this.state.name,
                category : this.state.category,
                location : this.state.location
            })
        }
    }
    render(){
        return <div className="title2">
            <input type="text" placeholder="name" onChange={this.handleChange} name="name" />
            <input type="text" placeholder="category" onChange={this.handleChange} name="category" />
            <input type="text" placeholder="location" onChange={this.handleChange} name="location" />
            <input type="button" value="add" onClick={this.add} />
        </div>
    }
}

export default OrganizationForm