import React, { Component } from 'react';

class FounderAddForm extends Component {
	constructor(props){
		super(props)
		this.state = {
            first_name : '',
            last_name : ''
		}
		this.handleChange = (evt) => {
			this.setState({
				[evt.target.name] : evt.target.value
			})
        }
        this.add = () => {
            this.props.onAdd({
                first_name : this.state.first_name,
        		last_name : this.state.last_name
            })
        }
    }
    render() {
        return (
        <div>
            <form>
                <label htmlFor="first_name">First name</label>
                <input type="text" name="first_name" id="first_name" onChange={this.handleChange}/>
                <label htmlFor="last_name">Last name</label>

                <input type="text" name="last_name" id="last_name" onChange={this.handleChange}/>
                <input type="button" value="+" onClick={this.add}/>
            </form>
        </div>
        )
    }
}

export default FounderAddForm
