import axios from 'axios'
import {EventEmitter} from 'fbemitter'


const SERVER='http://localhost:8080'

class OrganizationStore{
    constructor(){
        this.content = []
        this.emitter = new EventEmitter()
        this.selected = null
    }
    async getAll(){
        try {
            let response = await axios(`${SERVER}/organizations`)
			this.content = response.data
            this.emitter.emit('GET_ORGANIZATIONS_SUCCESS')
        } catch (err) {
            console.warn(err)
            this.emitter.emit('GET_ORGANIZATIONS_ERROR')
        }
    }
    async addOne(organization){
        try {
            await axios.post(`${SERVER}/organizations`, organization)
            await this.getAll()
        } catch (err) {
            console.warn(err)
            this.emitter.emit('ADD_ORGANIZATION_ERROR')
            }
    }
    async saveOne(id, organization){
        try {
            await axios.put(`${SERVER}/organizations/${id}`, organization)
            await this.getAll()
            this.emitter.emit('SAVE_ORGANIZATION_SUCCESS')
        } catch (err) {
            console.warn(err)
            this.emitter.emit('SAVE_ORGANIZATION_ERROR')
        }
    }
    async deleteOne(id){
        try {
            await axios.delete(`${SERVER}/organizations/${id}`)
            await this.getAll()
            this.emitter.emit('DELETE_ORGANIZATION_SUCCESS')
        } catch (err) {
            console.warn(err)
            this.emitter.emit('DELETE_ORGANIZATION_ERROR')
        }
    }
    async getOne(id){
        try {
            let response = await axios(`${SERVER}/organizations/${id}`)
            this.selected = response.data
            this.emitter.emit('GET_ONE_SUCCESS')
        } catch (e) {
            console.warn(e)
            this.emitter.emit('GET_ONE_ERROR')
        }
	}
}

export default OrganizationStore