import axios from 'axios'
import {EventEmitter} from 'fbemitter'

const SERVER = 'http://localhost:8080'

class FounderStore{
	constructor(){
		this.emitter = new EventEmitter()
		this.content = []
		this.selected = null
	}
	async addOne(organizationId, founder){
		try{
			await axios.post(`${SERVER}/organizations/${organizationId}/founders`, founder)
			await this.getAll(organizationId)
			this.emitter.emit('ADD_SUCCESS')
		}
		catch(ex){
			console.warn(ex)
			this.emitter.emit('ADD_ERROR')
		}
	}
	async getAll(organizationId){
		try{
			let response = await axios(`${SERVER}/organizations/${organizationId}/founders`)
			this.content = response.data
			this.emitter.emit('GET_ALL_SUCCESS')
		}
		catch(ex){
			console.warn(ex)
			this.emitter.emit('GET_ALL_ERROR')
		}
	}
	async deleteOne(organizationId, founderId){
        try {
            await axios.delete(`${SERVER}/organizations/${organizationId}/founders/${founderId}`)
            await this.getAll(organizationId)
            this.emitter.emit('DELETE_SUCCESS')
        } catch (e) {
            console.warn(e)
            this.emitter.emit('DELETE_ERROR')
        }
	}
	async saveOne(organizationId, founderId, founder){
        try {
            await axios.put(`${SERVER}/organizations/${organizationId}/founders/${founderId}`, founder)
            await this.getAll(organizationId)
            this.emitter.emit('SAVE_SUCCESS')
        } catch (e) {
            console.warn(e)
            this.emitter.emit('SAVE_ERROR')
        }
	}
	async getOne(organizationId, founderId){
        try {
            let response = await axios(`${SERVER}/organizations/${organizationId}/founders/${founderId}`)
            this.selected = response.data
            this.emitter.emit('GET_ONE_SUCCESS')
        } catch (e) {
            console.warn(e)
            this.emitter.emit('GET_ONE_ERROR')
        }
	}

}

export default FounderStore