import React, {Component} from 'react'

class Organization extends Component{
    constructor(props){
        super(props)
        this.state = {
            isEditing : false,
            name : this.props.item.name,
            category : this.props.item.category,
            location : this.props.item.location,
        }

        this.delete = () => {
            this.props.onDelete(this.props.item.id)
        }
        this.edit = () => {
            this.setState({isEditing : true})
        }
        this.cancel = () => {
            this.setState({isEditing : false})
        }
        this.handleChange = (evt) => {
            this.setState({
                [evt.target.name] : evt.target.value
            })
        }
        this.save = () => {
            this.props.onSave(this.props.item.id, {
                name : this.state.name,
                category : this.state.category,
                location : this.state.location
            })
            this.setState({
                isEditing : false
            })
        }
    }

    render(){
        if (this.state.isEditing){
            return <div>
                <h3><input type="text" value={this.state.name} name="name" onChange={this.handleChange} /></h3>
                <h5><input type="text" value={this.state.category} name="category" onChange={this.handleChange} /></h5>
                <h5><input type="text" value={this.state.location} name="location" onChange={this.handleChange} /></h5>
                <div>
                    <input type="button" value="cancel" onClick={this.cancel} />
                    <input type="button" value="save" onClick={this.save} />
                </div>
            </div>
        } else {
        return <div>
            <table>
                <tbody>
                    <tr>
                        <td>
                            <h5>{this.props.item.name}</h5>
                        </td>
                        <td>
                            <h5>{this.props.item.category}</h5>
                        </td>
                        <td>
                            <h5>{this.props.item.location}</h5>
                        </td>
                        <td>
                            <input id={'btn-delete'} type="button" value="delete" onClick={this.delete} />
                            <input type="button" value="edit" onClick={this.edit} />
                            <input type="button" value="details" onClick={() => this.props.onSelect(this.props.item.id)}/>
                        </td>
                    </tr>
                </tbody>
            </table>
                </div>            
        }
    }
}

export default Organization