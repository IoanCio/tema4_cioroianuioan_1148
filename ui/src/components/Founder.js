import React, { Component } from 'react';

class Founder extends Component {
	constructor(props){
		super(props)
		this.state = {
			isEditing : false,
            first_name : this.props.item.first_name,
            last_name : this.props.item.last_name
		}
		this.handleChange = (evt) => {
			this.setState({
				[evt.target.name] : evt.target.value
			})
		}
	}
  render() {
  	if (!this.state.isEditing){
	    return (
	      <div>I am a founder. My name is {this.props.item.first_name} {this.props.item.last_name} 
            <input type="button" value="delete" onClick={() => this.props.onDelete(this.props.item.id)} />
          	<input type="button" value="edit" onClick={() => this.setState({isEditing : true})} />
	      </div>
	    )  		
  	}
  	else{
  		return (
  			<div>I am a founder. My first name is  
                <input type="text" name="first_name" id="first_name" onChange={this.handleChange} value={this.state.first_name}/>
                and my last name is  
                <input type="text" name="last_name" id="last_name" onChange={this.handleChange} value={this.state.last_name}/>
                <input type="button" value="save" onClick={() => {
                    this.props.onSave(this.props.item.id, 
                        {
                            first_name : this.state.first_name,
                            last_name : this.state.last_name
                        }
                    )
		      		this.setState({isEditing : false})
		      	}
		      } />
		    	<input type="button" value="cancel" onClick={() => this.setState({isEditing : false})} />
	    	</div>
  		)
  	}
  }
}

export default Founder
