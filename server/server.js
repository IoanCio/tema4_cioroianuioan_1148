const express = require('express')
const bodyParser = require('body-parser')
const Sequelize = require('sequelize')
const cors = require('cors')


const sequelize = new Sequelize('org_db','Ioan','parola',{
	dialect : 'mysql',
	define : {
		timestamps : false
	}
})

const Organization = sequelize.define('organization', {
	name : Sequelize.STRING,
	category : Sequelize.STRING,
	location : Sequelize.STRING	
}, {
	underscored : true
})

const Founder = sequelize.define('founder', {
	first_name : Sequelize.STRING,
	last_name : Sequelize.STRING
})

Organization.hasMany(Founder)

const app = express()
app.use(cors())
app.use(bodyParser.json())
app.use(express.static('../ui/build'))

app.get('/create', async (req, res) => {
	try{
		await sequelize.sync({force : true})
		res.status(201).json({message : 'created'})
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.get('/organizations', async (req, res) => {
	try{
		let organizations = await Organization.findAll()
		res.status(200).json(organizations)
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})


app.post('/organizations', async (req, res) => {
	try{
		if (req.query.bulk && req.query.bulk == 'on'){
			await Organization.bulkCreate(req.body)
			res.status(201).json({message : 'created'})
		}
		else{
			await Organization.create(req.body)
			res.status(201).json({message : 'created'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.get('/organizations/:id', async (req, res) => {
	try{
		let organization = await Organization.findByPk(req.params.id)
		if (organization){
			res.status(200).json(organization)
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.put('/organizations/:id', async (req, res) => {
	try{
		let organization = await Organization.findByPk(req.params.id)
		if (organization){
			await organization.update(req.body)
			res.status(202).json({message : 'accepted'})
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.delete('/organizations/:id', async (req, res) => {
	try{
		let organization = await Organization.findByPk(req.params.id)
		if (organization){
			await organization.destroy()
			res.status(202).json({message : 'accepted'})
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.get('/organizations/:oid/founders', async (req, res) => {
	try{
		let organization = await Organization.findByPk(req.params.oid)
		if (organization){
			let founders = await organization.getFounders()
			res.status(200).json(founders)
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.get('/organizations/:oid/founders/:fid', async (req, res) => {
	try{
		let organization = await Organization.findByPk(req.params.oid)
		if (organization){
			let founders = await organization.getFounders({where : {id : req.params.fid}})
			res.status(200).json(founders.shift())
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.post('/organizations/:oid/founders', async (req, res) => {
	try{
		let organization = await Organization.findByPk(req.params.oid)
		if (organization){
			let founder = req.body
			founder.organizationId = organization.id
			await Founder.create(founder)
			res.status(201).json({message : 'created'})
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.put('/organizations/:oid/founders/:fid', async (req, res) => {
	try{
		let organization = await Organization.findByPk(req.params.oid)
		if (organization){
			let founders = await organization.getFounders({where : {id : req.params.fid}})
			let founder = founders.shift()
			if (founder){
				await founder.update(req.body)
				res.status(202).json({message : 'accepted'})
			}
			else{
				res.status(404).json({message : 'not found'})
			}
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.delete('/organizations/:oid/founders/:fid', async (req, res) => {
	try{
		let organization = await Organization.findByPk(req.params.oid)
		if (organization){
			let founders = await organization.getFounders({where : {id : req.params.fid}})
			let founder= founders.shift()
			if (founder){
				await founder.destroy(req.body)
				res.status(202).json({message : 'accepted'})
			}
			else{
				res.status(404).json({message : 'not found'})
			}
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.listen(8080)